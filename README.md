# queue-agnos [![Build Status](https://travis-ci.org/anonymint/queue-agnos.svg?branch=master)](https://travis-ci.org/anonymint/queue-agnos)
Queue implementation environment agnostic within-JVM, with-multiple-JVM, with SQS-AWS 

### Idea
Basically we want to have development environment of AWS in particular SQS so developer can run test, debug as if they run SQS locally 
by abstracting away SQS via interface operations.

### Requirements 
* Thread safe
* Test time based operation by mocking DateTime
* Mock and mock and mock everything even file system.

### JVM version
Using Map as a queue and thread mechanism to prevent race condition.

### Multiple JVM version
Using POSIX manipulate linux folder as mechanism to acquire and release lock.

### SQS AWS
AWS provides all mechanism that we need, just use it via interface.
