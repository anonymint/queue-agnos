package com.example;

import com.example.domain.MessageWrapper;
import com.google.common.base.Splitter;
import com.google.common.hash.Hashing;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.time.Clock;
import java.util.Base64;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileQueueService implements QueueService {

    private static final String QUEUE_FILE = "message";
    private static final String QUEUE_TEMP_FILE = "temp";
    private static final String LOCK_FOLDER = ".lock";
    private final Path queueStartLocation;
    private final Clock clock;
    private final Integer defaultVisibilityTimeout;

    public FileQueueService(Path queueStartLocation, Clock clock, Integer defaultVisibilityTimeout) {
        this.queueStartLocation = queueStartLocation;
        this.clock = clock;
        this.defaultVisibilityTimeout = defaultVisibilityTimeout;
    }

    public FileQueueService() {
        this(Paths.get("/tmp/sqs"), Clock.systemDefaultZone(), 2); //default at tmp location linux please
    }

    /**
     * Append message at the end of file
     *
     * @param queueUrl
     * @param delaySeconds delaySeconds must be (0 to 900 - 15 minutes) or null(default value applied)
     * @param message
     * @throws InterruptedException
     * @throws IOException
     */

    @Override
    public void push(final String queueUrl, final Integer delaySeconds, final String message) throws InterruptedException, IOException {
        //check edge case
        validateString(queueUrl, message);
        validateDelaySeconds(delaySeconds);

        Path queue = fromUrl(queueUrl);
        Path lock = getLockFile(queue);

        long visibilityFrom = delaySeconds != null ? now() + delaySeconds * 1000 : 0L;

        lock(lock);
        Path messages = getMessagesFile(queue);
        try (BufferedWriter pw = Files.newBufferedWriter(messages, StandardCharsets.UTF_8, StandardOpenOption.APPEND)) {
            MessageWrapper messageWrapper = new MessageWrapper(message);
            pw.write(formatRecord(messageWrapper.getMessageId(), messageWrapper.getBody(), visibilityFrom));
            pw.newLine();
        } finally {
            unlock(lock);
        }
    }

    /**
     * Pull message from Queue and put inline message back with availability time
     * <p>
     * The mechanism is to create a temp file with all messages from original file except the return message
     * that change the visibility time to currentTime + visibilityTimeout
     * then last step to replace original file with temp file
     *
     * @param queueUrl queue location
     * @return Optional MessageWrapper object or empty
     * @throws InterruptedException
     * @throws IOException
     * @throws IllegalArgumentException
     */
    @Override
    public Optional<MessageWrapper> pull(final String queueUrl, final Integer visibilityTimeout) throws InterruptedException, IOException {
        //check edge case
        validateString(queueUrl);
        validateVisibilityTimeout(visibilityTimeout);

        //if visibilityTimeout provided
        int visibility = visibilityTimeout != null ? visibilityTimeout : defaultVisibilityTimeout;

        Path queue = fromUrl(queueUrl);
        Path lock = getLockFile(queue);

        lock(lock);
        Path messages = getMessagesFile(queue);
        Path tempMessages = getTempMessagesFile(queue);
        MessageWrapper messageWrapper = null;
        try (Stream<String> stream = Files.lines(messages)) {
            List<String> lines = stream.collect(Collectors.toList());
            long currentTime = now();

            try (BufferedWriter pw = Files.newBufferedWriter(tempMessages, StandardCharsets.UTF_8, StandardOpenOption.APPEND)) {
                for (String line : lines) {
                    List<String> splitter = Splitter.on(SEPARATE_CHAR).limit(5).splitToList(line);
                    if (Objects.isNull(messageWrapper) &&
                            (splitter.get(1).equals("0") ||
                                    Long.valueOf(splitter.get(1)) < currentTime)) {
                        int readAttempt = Integer.valueOf(splitter.get(0));
                        String messageId = splitter.get(3);
                        String content = splitter.get(4);
                        String decodeBody = new String(Base64.getDecoder().decode(content));
                        long visibleFrom = currentTime + visibility * 1000;
                        messageWrapper = new MessageWrapper(messageId, decodeBody);
                        pw.write(formatRecord(messageId, messageWrapper.getBody(), messageWrapper.getReceiptHandle(),
                                visibleFrom, ++readAttempt));
                        pw.newLine();
                    } else {
                        pw.write(line);
                        pw.newLine();
                    }
                }
            }
            Files.move(tempMessages, messages, StandardCopyOption.REPLACE_EXISTING);
        } finally {
            unlock(lock);
        }
        return Objects.isNull(messageWrapper) ? Optional.empty() : Optional.of(messageWrapper);
    }

    /**
     * Delete message from Queue
     * <p>
     * The mechanism is almost as same as Pull, filter remove message from original file to temp file,
     * then replace original with temp file
     *
     * @param queueUrl      queue location
     * @param receiptHandle receiptHandle to remove from Queue
     * @throws InterruptedException
     * @throws IOException
     * @throws IllegalArgumentException if queueUrl or HelperMessage are illegal format
     */
    @Override
    public void delete(final String queueUrl, final String receiptHandle) throws InterruptedException, IOException {
        //check edge case
        validateString(queueUrl, receiptHandle);

        Path queue = fromUrl(queueUrl);
        Path lock = getLockFile(queue);

        lock(lock);
        Path messages = getMessagesFile(queue);
        Path tempMessages = getTempMessagesFile(queue);
        try (Stream<String> stream = Files.lines(messages)) {
            List<String> lines = stream
                    .filter(s -> !Splitter.on(SEPARATE_CHAR).splitToList(s).get(2).equals(receiptHandle))
                    .collect(Collectors.toList());

            Files.write(tempMessages, lines, StandardCharsets.UTF_8);
            Files.move(tempMessages, messages, StandardCopyOption.REPLACE_EXISTING);
        } finally {
            unlock(lock);
        }
    }

    /**
     * Inject java Clock class here, so we can mock it
     *
     * @return current EpochMilli in long
     */
    private long now() {
        return clock.instant().toEpochMilli();
    }

    private Path fromUrl(final String queueUrl) throws IOException {
        //String queue = queueUrl.indexOf('/') == 0 ? queueUrl.substring(1) : queueUrl;
        String queue = Hashing.sha1()
            .newHasher()
            .putString(queueUrl, Charset.defaultCharset())
            .hash()
            .toString();
        Path queuePath = queueStartLocation.resolve(queue);
        Files.createDirectories(queuePath);
        return queuePath;
    }

    private Path getMessagesFile(final Path queue) throws IOException {
        Path message = queue.resolve(QUEUE_FILE);
        if (!Files.exists(message)) {
            Files.createFile(message);
        }
        return message;
    }

    private Path getTempMessagesFile(final Path queue) throws IOException {
        Path temp = queue.resolve(QUEUE_TEMP_FILE);
        if (!Files.exists(temp)) {
            Files.createFile(temp);
        }
        return temp;
    }

    private Path getLockFile(final Path queue) throws IOException {
        return queue.resolve(LOCK_FOLDER);
    }

    /**
     * Lock on path by atomic create directory, if fail throw exception keep doing it
     *
     * @param lock lock folder Path location
     * @throws InterruptedException
     */
    private void lock(final Path lock) throws InterruptedException {
        while (true) {
            try {
                Files.createDirectory(lock);
                break;
            } catch (IOException e) {
                Thread.sleep(50);
            }
        }
    }

    /**
     * Unlock by removing lock path so other threads can get lock
     *
     * @param lock lock folder Path location
     * @throws IOException
     */
    private void unlock(final Path lock) throws IOException {
        Files.delete(lock);
    }

}
