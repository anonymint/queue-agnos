package com.example;

import com.example.domain.MessageWrapper;
import com.google.common.base.Splitter;

import java.time.Clock;
import java.util.*;
import java.util.Base64;
import java.util.concurrent.*;

public class InMemoryQueueService implements QueueService {

    private final ConcurrentMap<String, List<String>> queueMap;
    private final Clock clock;
    private final Integer defaultVisibilityTimeout;

    public InMemoryQueueService() {
        this(new ConcurrentHashMap<>(), Clock.systemDefaultZone(), 2); //default param with 2 sec visibilityTimeout
    }

    public InMemoryQueueService(ConcurrentMap<String, List<String>> queueMap, Clock clock, Integer defaultVisibilityTimeout) {
        this.queueMap = queueMap;
        this.clock = clock;
        this.defaultVisibilityTimeout = defaultVisibilityTimeout;
    }

    /**
     * Push message to synchronized block of process adding to List
     *
     * @param queueUrl
     * @param delaySeconds delaySeconds must be (0 to 900 - 15 minutes) or null(default value applied)
     * @param message
     */
    @Override
    public void push(final String queueUrl, final Integer delaySeconds, final String message) {
        //check edge case
        validateString(queueUrl, message);
        validateDelaySeconds(delaySeconds);

        synchronized (fromUrl(queueUrl)) {
            long visibilityFrom = delaySeconds != null ? now() + delaySeconds * 1000 : 0L;
            MessageWrapper messageWrapper = new MessageWrapper(message);
            fromUrl(queueUrl).add(formatRecord(messageWrapper.getMessageId(), messageWrapper.getBody(), visibilityFrom));
        }
    }

    /**
     * Read line and put message with visibleFrom time with number of readAttempt
     *
     * @param queueUrl
     * @param visibilityTimeout must be greater than 0 or null(default value applied)
     * @return Optional MessageWrapper object or empty
     */
    @Override
    public Optional<MessageWrapper> pull(final String queueUrl, final Integer visibilityTimeout) {
        //check edge case
        validateString(queueUrl);
        validateVisibilityTimeout(visibilityTimeout);

        synchronized (fromUrl(queueUrl)) {
            List<String> list = fromUrl(queueUrl);
            long currentTime = now();
            //if visibilityTimeout provided
            int visibility = visibilityTimeout != null ? visibilityTimeout : defaultVisibilityTimeout;

            for (int i = 0; i < list.size(); i++) {
                List<String> splitter = Splitter.on(SEPARATE_CHAR).limit(5).splitToList(list.get(i));
                if (splitter.get(1).equals("0") ||
                        Long.valueOf(splitter.get(1)) < currentTime) {
                    int readAttempt = Integer.valueOf(splitter.get(0));
                    String messageId = splitter.get(3);
                    String content = splitter.get(4);
                    String decodeBody = new String(Base64.getDecoder().decode(content));
                    long visibleFrom = currentTime + visibility * 1000;
                    MessageWrapper messageWrapper = new MessageWrapper(messageId, decodeBody);
                    list.set(i, formatRecord(messageId, decodeBody, messageWrapper.getReceiptHandle(), visibleFrom, ++readAttempt));
                    return Optional.of(messageWrapper);
                }
            }
        }
        return Optional.empty();

    }

    /**
     * Delete message from queueUrl with provided receiptHandle
     *
     * @param queueUrl      queue location
     * @param receiptHandle receiptHandle id to remove from queue
     * @throws IllegalArgumentException if queueUrl or receiptHandle is illegal format
     */
    @Override
    public void delete(final String queueUrl, final String receiptHandle) {
        //check edge case
        validateString(queueUrl, receiptHandle);

        synchronized (fromUrl(queueUrl)) {
            List<String> list = fromUrl(queueUrl);
            for (int i = 0; i < list.size(); i++) {
                if (Splitter.on(SEPARATE_CHAR).splitToList(list.get(i)).get(2).equals(receiptHandle)) {
                    list.remove(i);
                    break;
                }
            }
        }
    }

    /**
     * Get queue if exists or create a new one and return
     *
     * @param queueUrl queue location
     * @return Queue object according to queueUrl, if doesn't exist, create an empty one
     */
    private List<String> fromUrl(final String queueUrl) {
        if (queueMap.containsKey(queueUrl)) return queueMap.get(queueUrl);

        synchronized (queueMap) {
          if (!queueMap.containsKey(queueUrl)) {
            queueMap.put(queueUrl, new ArrayList<>());
          }
        }

        return queueMap.get(queueUrl);
    }

    /**
     * Inject java Clock class here, so we can mock it
     *
     * @return current EpochMilli in long
     */
    private long now() {
        return clock.instant().toEpochMilli();
    }

}
