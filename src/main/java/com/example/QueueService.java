package com.example;

import com.example.domain.MessageWrapper;
import com.google.common.base.Joiner;
import com.google.common.base.Strings;

import java.io.IOException;
import java.util.Base64;
import java.util.Optional;

public interface QueueService {

    String SEPARATE_CHAR = ":";

    default void validateString(String... messages) {
        for (String s : messages) {
            if (Strings.isNullOrEmpty(s))
                throw new IllegalArgumentException("please check string param, either null or blank content");
        }
    }

    default void validateDelaySeconds(final Integer delaySeconds) {
        if (delaySeconds != null && (delaySeconds < 0 || delaySeconds > 900))
            throw new IllegalArgumentException("The number of delaySeconds must be (0 to 900 - 15 minutes) or null(default value applied)");
    }

    default void validateVisibilityTimeout(final Integer visibilityTimeout) {
        if (visibilityTimeout != null && visibilityTimeout < 0)
            throw new IllegalArgumentException("The number of visibilityTimeout must greater than 0 or null(default value applied)");
    }

    default String formatRecord(final String messageId, final String message, final long visibleFrom) {
        return formatRecord(messageId, message, null, visibleFrom, 0);
    }

    default String formatRecord(final String messageId, final String message,
                                final String receiptHandle, final long visibleFrom, final int count) {
        String encodedContent = new String(Base64.getEncoder().encode(message.getBytes()));
        return Joiner
                .on(SEPARATE_CHAR)
                .useForNull("")
                .join(count, visibleFrom, receiptHandle, messageId, encodedContent);
    }

    void push(String queueUrl, Integer delaySeconds, String message) throws InterruptedException, IOException;

    Optional<MessageWrapper> pull(String queueUrl, Integer visibilityTimeout) throws InterruptedException, IOException;

    void delete(String queueUrl, String receiptHandle) throws InterruptedException, IOException;

}
