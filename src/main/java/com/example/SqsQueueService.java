package com.example;

import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.*;
import com.example.domain.MessageWrapper;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

public class SqsQueueService implements QueueService {

    private final AmazonSQSClient client;
    private final Integer defaultVisibilityTimeout;

    public SqsQueueService(AmazonSQSClient sqsClient, Integer defaultVisibilityTimeout) {
        this.client = sqsClient;
        this.defaultVisibilityTimeout = defaultVisibilityTimeout;
    }

    @Override
    public void push(final String queueUrl, final Integer delaySeconds, final String message) throws IOException {
        //check edge case
        validateString(queueUrl, message);
        validateDelaySeconds(delaySeconds);

        //if delaySecond provided
        int delay = delaySeconds != null ? delaySeconds : 0;

        SendMessageRequest sendMessageRequest = new SendMessageRequest()
                .withQueueUrl(fromUrl(queueUrl))
                .withDelaySeconds(delay)
                .withMessageBody(message);
        client.sendMessage(sendMessageRequest);
    }

    @Override
    public Optional<MessageWrapper> pull(final String queueUrl, final Integer visibilityTimeout) throws IOException {
        //check edge case
        validateString(queueUrl);
        validateVisibilityTimeout(visibilityTimeout);

        //if visibilityTimeout provided
        int visibility = visibilityTimeout != null ? visibilityTimeout : defaultVisibilityTimeout;

        ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest()
                .withQueueUrl(fromUrl(queueUrl))
                .withVisibilityTimeout(visibility);
        receiveMessageRequest.setMaxNumberOfMessages(1); //get message one at the time
        ReceiveMessageResult receiveMessageResult = client.receiveMessage(receiveMessageRequest);
        List<Message> messages = receiveMessageResult.getMessages();
        return (messages == null || messages.size() == 0) ? Optional.empty() : Optional.of(new MessageWrapper(messages.get(0)));
    }


    @Override
    public void delete(final String queueUrl, final String receiptHandle) throws IOException {
        //check edge case
        validateString(queueUrl, receiptHandle);

        DeleteMessageRequest deleteMessageRequest = new DeleteMessageRequest()
                .withQueueUrl(fromUrl(queueUrl))
                .withReceiptHandle(receiptHandle);
        client.deleteMessage(deleteMessageRequest);
    }

    private String fromUrl(final String queueUrl) {
        return client.createQueue(queueUrl).getQueueUrl();
    }

}
