package com.example.domain;

import com.amazonaws.services.sqs.model.Message;
import com.google.common.hash.Hashing;

import java.nio.charset.Charset;
import java.time.Instant;
import java.util.UUID;

/**
 * MessageWrapper Object to represent abstract Model of SQS
 * <p>
 * This will abstract SQS message away
 *
 * @author <a href="www.3kalak.com">anonymint</a>
 * @since 2016-07-07
 */
public class MessageWrapper extends Message {

    /**
     * Create new messageID
     *
     * @param body
     */
    public MessageWrapper(String body) {
        this(UUID.randomUUID().toString(), body);
    }

    /**
     * Create old messageId
     *
     * @param messageId
     * @param body
     */
    public MessageWrapper(String messageId, String body) {
        this.withMessageId(messageId)
                .withBody(body)
                .withMD5OfBody(Hashing.md5()
                        .newHasher()
                        .putString(body, Charset.defaultCharset())
                        .hash()
                        .toString())
                .withReceiptHandle(Hashing.sha256()
                        .newHasher()
                        .putString(getMessageId(), Charset.defaultCharset())
                        .putString(body, Charset.defaultCharset())
                        .putLong(Instant.now().getEpochSecond())
                        .hash()
                        .toString());
    }

    /**
     * Helper constructor to convert AWS SQS Message to MessageWrapper
     *
     * @param message
     */
    public MessageWrapper(Message message) {
        this.withMessageId(message.getMessageId())
                .withBody(message.getBody())
                .withMD5OfBody(message.getMD5OfBody())
                .withReceiptHandle(message.getReceiptHandle())
                .withAttributes(message.getAttributes())
                .withMessageAttributes(message.getMessageAttributes())
                .withMD5OfMessageAttributes(message.getMD5OfMessageAttributes());
    }

}
