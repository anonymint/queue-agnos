package com.example.run;

import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.example.FileQueueService;
import com.example.InMemoryQueueService;
import com.example.QueueService;
import com.example.SqsQueueService;
import com.example.domain.MessageWrapper;

import java.io.IOException;
import java.util.Optional;
import java.util.concurrent.*;

/**
 * Class to run Example multi Producer and Consumer - testing only do not ship!
 *
 * @author <a href="www.3kalak.com">anonymint</a>
 * @since 2016-07-07
 */
public class Main {
    private ScheduledExecutorService executorProducer = Executors.newScheduledThreadPool(10);
    private ScheduledExecutorService executorConsumer = Executors.newScheduledThreadPool(20);
    private static QueueService queue;

    private Runnable pushRun = (() -> {
        try {
            int randomNumber = (int) (Math.random() * 5) + 1;
            String queueNumber = "sqs-queue-" + randomNumber;
            queue.push(queueNumber, randomNumber, Thread.currentThread().getName() + " for " + randomNumber + " sec delay for queue " + queueNumber);
            System.out.println(String.format("Push message to queue %s", queueNumber));
        } catch (InterruptedException | IOException e) {
            e.printStackTrace();
        }
    });

    private Runnable pullRun = (() -> {
        try {
            int randomNumber = (int) (Math.random() * 5) + 1;
            String queueNumber = "sqs-queue-" + randomNumber;
            Optional<MessageWrapper> m = queue.pull(queueNumber, randomNumber);
            if (m.isPresent()) {
                System.out.println(String.format("From queue %s Pull Only %s", queueNumber, m.get()));
            }
        } catch (InterruptedException | IOException e) {
            e.printStackTrace();
        }
    });

    private Runnable pullAndRemoveRun = (() -> {
        try {
            int randomNumber = (int) (Math.random() * 5) + 1;
            String queueNumber = "sqs-queue-" + randomNumber;
            Optional<MessageWrapper> m = queue.pull(queueNumber, randomNumber);
            if (m.isPresent()) {
                System.out.println(String.format("From queue %s Pull and Remove %s", queueNumber, m.get()));
                queue.delete(queueNumber, m.get().getReceiptHandle());
            }
        } catch (InterruptedException | IOException e) {
            e.printStackTrace();
        }
    });

    private static AmazonSQSClient getClient() {
        /*
         * The ProfileCredentialsProvider will return your [default]
         * credential profile by reading from the credentials file located at
         * (~/.aws/credentials).
         */
        AWSCredentials credentials;
        try {
            credentials = new ProfileCredentialsProvider().getCredentials();
        } catch (Exception e) {
            throw new AmazonClientException(
                    "Cannot load the credentials from the credential profiles file. " +
                            "Please make sure that your credentials file is at the correct " +
                            "location (~/.aws/credentials), and is in valid format.",
                    e);
        }

        AmazonSQSClient sqs = new AmazonSQSClient(credentials);
        Region usWest2 = Region.getRegion(Regions.AP_SOUTHEAST_2);
        sqs.setRegion(usWest2);
        return sqs;
    }

    public static void main(String[] args) {
        Main run = new Main();

        if (args.length < 1) {
            System.out.println("Please provide input [1] inMemory, [2] file and [3] AWS SQS(you need proper setup)");
            System.exit(1);
        }

        try {
            switch (args[0]) {
                case "1":
                    queue = new InMemoryQueueService();
                    break;
                case "2":
                    queue = new FileQueueService();
                    break;
                case "3":
                    queue = new SqsQueueService(getClient(), 0);
                    break;
                default:
                    System.out.println("Parameter are 1,2 or 3");
                    System.exit(1);
            }
            run.run(queue);
        } catch (InterruptedException | IOException e) {
            e.printStackTrace();
        }
    }

    private void run(QueueService queue) throws InterruptedException, IOException {
        //push run every half sec
        int ProducerNum = 5;
        for (int i = 0; i < ProducerNum; i++) {
            executorProducer.scheduleAtFixedRate(pushRun, 0, 50, TimeUnit.MILLISECONDS);
        }
        //pull run but not delete, so message will comeback
        int consumerPullOnlyNum = 5;
        for (int i = 0; i < consumerPullOnlyNum; i++) {
            executorConsumer.scheduleAtFixedRate(pullRun, 0, 10, TimeUnit.MILLISECONDS);
        }

        //pull and delete
        int consumerPullDeleteNum = 15;
        for (int i = 0; i < consumerPullDeleteNum; i++) {
            executorConsumer.scheduleAtFixedRate(pullAndRemoveRun, 0, 10, TimeUnit.MILLISECONDS);
        }
    }

}
