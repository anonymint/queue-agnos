package com.example;

import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertTrue;

/**
 * IllegalArgument test for all implementation
 *
 * @author <a href="www.3kalak.com">anonymint</a>
 * @since 2016-07-07
 */
public abstract class AbstractIllegalArgumentTest<T extends QueueService> {

    private T service;

    protected abstract T createInstance();

    protected Integer DEFAULT_VISIBILITY_TIMEOUT = 2;

    @Before
    public void initial() {
        service = createInstance();
    }

    @Test(timeout = 500)
    public void check_illegal_push() throws IOException {
        //null queueUrl check
        try {
            service.push(null, 2, "Valid String");
        } catch (Exception e) {
            assertTrue(e instanceof IllegalArgumentException);
        }

        //empty queueUrl check
        try {
            service.push("", 2, "Valid String");
        } catch (Exception e) {
            assertTrue(e instanceof IllegalArgumentException);
        }

        //illegal delaySeconds - valid one is (0-900 - 15 min) or null
        try {
            service.push("valid-queue", 901, "Valid String");
        } catch (Exception e) {
            assertTrue(e instanceof IllegalArgumentException);
        }

        //illegal delaySeconds - valid one is (0-900 - 15 min) or null
        try {
            service.push("valid-queue", -1, "Valid String");
        } catch (Exception e) {
            assertTrue(e instanceof IllegalArgumentException);
        }
    }

    @Test(timeout = 500)
    public void check_illegal_pull() throws IOException {
        //null queueUrl check
        try {
            service.pull(null, 10);
        } catch (Exception e) {
            assertTrue(e instanceof IllegalArgumentException);
        }

        //empty queueUrl check
        try {
            service.pull("", 10);
        } catch (Exception e) {
            assertTrue(e instanceof IllegalArgumentException);
        }

        //invalid value of visibility timeout
        try {
            service.pull("valid-queue", -2);
        } catch (Exception e) {
            assertTrue(e instanceof IllegalArgumentException);
        }
    }

    @Test(timeout = 500)
    public void check_illegal_delete() throws IOException {
        //null queueUrl check
        try {
            service.delete(null, "Valid String");
        } catch (Exception e) {
            assertTrue(e instanceof IllegalArgumentException);
        }

        //empty queueUrl check
        try {
            service.delete("", "Valid String");
        } catch (Exception e) {
            assertTrue(e instanceof IllegalArgumentException);
        }

        //null receiptHandle check
        try {
            service.delete("valid-queue", null);
        } catch (Exception e) {
            assertTrue(e instanceof IllegalArgumentException);
        }

        //empty receiptHandle check
        try {
            service.delete("valid-queue", "");
        } catch (Exception e) {
            assertTrue(e instanceof IllegalArgumentException);
        }
    }

}
