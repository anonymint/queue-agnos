package com.example;

import com.example.domain.MessageWrapper;
import com.google.common.jimfs.Configuration;
import com.google.common.jimfs.Jimfs;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;
import java.nio.file.*;
import java.time.Clock;
import java.time.Instant;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class FileQueueTest extends AbstractIllegalArgumentTest {

  @Mock
  private Clock clock;

  private FileQueueService service;

  //FileSystem memory provider, we run unittest in FileSystem memory, sort of Mock
  private FileSystem fs = Jimfs.newFileSystem(Configuration.unix());

  @Before
  public void setUp() {
    MockitoAnnotations.initMocks(this);
    Path root = fs.getPath("/root/sqs");
    service = new FileQueueService(root, clock, DEFAULT_VISIBILITY_TIMEOUT);
  }

  @Override
  protected QueueService createInstance() {
    return new FileQueueService(mock(Path.class), clock, DEFAULT_VISIBILITY_TIMEOUT);
  }

  @Test
  public void check_push_message() throws InterruptedException, IOException {
    //given
    String queueUrl = "/test-queue";

    //when
    doReturn(Instant.now()).when(clock).instant();
    service.push(queueUrl, null, "m1\nm1");
    service.push(queueUrl, null, "\nm2");
    service.push(queueUrl, null, "m3\n");

    //then
    assertEquals(service.pull(queueUrl, null).get().getBody(), "m1\nm1");
    assertEquals(service.pull(queueUrl, null).get().getBody(), "\nm2");
    assertEquals(service.pull(queueUrl, null).get().getBody(), "m3\n");
  }

  @Test(timeout = 500)
  public void check_remove_message() throws InterruptedException, IOException {
    //given
    String queueUrl = "/test-remove";
    String message = "m1";
    int timeout = 3; // visibility-timeout = 3s
    Instant now = Instant.now();
    Instant plus3Sec = now.plusSeconds(4);

    //when
    when(clock.instant()).thenReturn(now, plus3Sec);
    service.push(queueUrl, timeout, message);
    MessageWrapper m2 = service.pull(queueUrl, null).get();
    assertEquals(m2.getBody(), message);
    service.delete(queueUrl, m2.getReceiptHandle());

    //then
    //after delete we shouldn't see anything left, pull will run with "plus3Sec"
    assertFalse(service.pull(queueUrl, null).isPresent());
  }

  @Test(timeout = 500)
  public void check_visibility_timeout() throws InterruptedException, IOException {
    //given
    String queueUrl = "/test-visibility-timeout";
    String message = "m1";
    int visibilityTimeout = 2; // visibility-timeout = 2s
    Instant now = Instant.now();
    Instant plus3Sec = now.plusSeconds(3);
    Instant plus6Sec = now.plusSeconds(6);

    //when
    when(clock.instant()).thenReturn(now, plus3Sec, plus6Sec);
    service.push(queueUrl, null, message);
    service.pull(queueUrl, visibilityTimeout);

    //then
    //after 3 sec, we will be able to pull same message back
    Optional<MessageWrapper> m2 = service.pull(queueUrl, visibilityTimeout);
    assertTrue(m2.isPresent());
    assertEquals(message, m2.get().getBody());

    //after 6 sec, we will get same message again
    Optional<MessageWrapper> m3 = service.pull(queueUrl, visibilityTimeout);
    assertTrue(m3.isPresent());
    assertEquals(message, m3.get().getBody());
  }

}
