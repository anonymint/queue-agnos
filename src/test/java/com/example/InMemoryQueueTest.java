package com.example;

import com.example.domain.MessageWrapper;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.Clock;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.*;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class InMemoryQueueTest extends AbstractIllegalArgumentTest {

    @Mock
    private Clock clock;

    private InMemoryQueueService service;

    private ConcurrentMap<String, List<String>> queue;

    @Override
    protected QueueService createInstance() {
        return new InMemoryQueueService(queue, clock, DEFAULT_VISIBILITY_TIMEOUT);
    }

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        queue = new ConcurrentHashMap<>();
        service = new InMemoryQueueService(queue, clock, DEFAULT_VISIBILITY_TIMEOUT);
    }

    @Test//(timeout = 500)
    public void check_basic_operation_push_pull() throws Exception {
        //given
        String queue1 = "/queue1";
        String queue2 = "/queue2";
        String message1 = "message for queue 1";
        String message2 = "message for queue 2";
        Instant now = Instant.now();


        //when
        when(clock.instant()).thenReturn(now, now, now, now);
        service.push(queue1, null, message1);
        service.push(queue2, null, message2);

        //then
        assertTrue("Expect to have 2 queues", queue.size() == 2);
        assertTrue("Queue1 does exist and has size 1", queue.containsKey(queue1) && queue.get(queue1).size() == 1);
        assertTrue("Queue2 does exist and has size 1", queue.containsKey(queue2) && queue.get(queue2).size() == 1);
        assertEquals("Pull message out expect to get same result",
                message1, service.pull(queue1, null).get().getBody());
        assertEquals("Pull message out expect to get same result",
                message2, service.pull(queue2, null).get().getBody());
    }

    /**
     * HelperMessage will come back, if not delete
     */
    @Test(timeout = 500)
    public void check_visibility_timeout() throws InterruptedException {
        //given
        String url = "/testVisibilityTimeout";
        String m1 = "m1";
        int visibilityTimeout = 10;
        Instant now = Instant.now();
        Instant plus11Sec = now.plusSeconds(11);

        //when
        when(clock.instant()).thenReturn(now, now, plus11Sec);
        service.push(url, null, m1);
        Optional<MessageWrapper> firstPull = service.pull(url, visibilityTimeout);
        Optional<MessageWrapper> secondPull = service.pull(url, visibilityTimeout);
        Optional<MessageWrapper> pull11SecondLater = service.pull(url, visibilityTimeout);

        //then
        assertTrue("First Pull must return data", firstPull.isPresent());
        assertEquals(m1, firstPull.get().getBody());
        assertFalse("Second Pull must not return data", secondPull.isPresent());
        assertTrue("11 seconds later we must get data", pull11SecondLater.isPresent());
        assertEquals(m1, pull11SecondLater.get().getBody());
    }

    /**
     * HelperMessage won't append back to queue, if delete
     */
    @Test(timeout = 500)
    public void check_delete() throws InterruptedException {
        //given
        String url = "/testDelete";
        String messageToDelete = "HelperMessage to be deleted";
        int visibilityTimeout = 2;
        Instant now = Instant.now();
        Instant plus3Sec = now.plusSeconds(3);

        //when
        when(clock.instant()).thenReturn(now, now, plus3Sec);
        service.push(url, null, messageToDelete);
        Optional<MessageWrapper> m2 = service.pull(url, visibilityTimeout);
        m2.ifPresent(s -> service.delete(url, s.getReceiptHandle()));
        Optional<MessageWrapper> m3 = service.pull(url, visibilityTimeout);

        //then
        // message to be remove from pull must be the same as push
        assertTrue(m2.isPresent());
        assertEquals(messageToDelete, m2.get().getBody());
        assertFalse(m3.isPresent());
    }

}
