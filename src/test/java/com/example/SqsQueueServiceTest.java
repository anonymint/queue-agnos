package com.example;

import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.*;
import com.google.common.base.Joiner;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * SqsQueueService Test class
 *
 * @author <a href="www.3kalak.com">anonymint</a>
 * @since 2016-07-07
 */
public class SqsQueueServiceTest extends AbstractIllegalArgumentTest {

    private SqsQueueService service;

    @Mock
    private AmazonSQSClient client;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        service = new SqsQueueService(client, DEFAULT_VISIBILITY_TIMEOUT);
    }

    @Override
    protected QueueService createInstance() {
        return new SqsQueueService(client, DEFAULT_VISIBILITY_TIMEOUT);
    }

    @Test(timeout = 500)
    public void check_push() throws IOException {
        //given
        String queueUrl = "queue-for-push";
        String actualAwsQueueUrl = Joiner.on("/").join("aws", queueUrl);
        String message = "this-is-a-message";
        Integer delaySeconds = (int) (Math.random() * 500);
        ArgumentCaptor<SendMessageRequest> argumentCaptor = ArgumentCaptor.forClass(SendMessageRequest.class);
        mockCreateQueueResult(queueUrl, actualAwsQueueUrl); //mock create queue

        //when
        service.push(queueUrl, delaySeconds, message);

        //then
        verify(client).sendMessage(argumentCaptor.capture());
        assertEquals(1, argumentCaptor.getAllValues().size());

        SendMessageRequest sendMessageRequest = argumentCaptor.getAllValues().get(0);
        assertEquals(actualAwsQueueUrl, sendMessageRequest.getQueueUrl()); //assert it pushes from aws actual url
        assertEquals(message, sendMessageRequest.getMessageBody());
        assertEquals(delaySeconds, sendMessageRequest.getDelaySeconds());
    }

    @Test(timeout = 500)
    public void check_pull() throws IOException {
        String queueUrl = "queue-for-pull";
        String actualAwsQueueUrl = Joiner.on("/").join("aws", queueUrl);
        Integer visibilityTimeout = (int) (Math.random() * 12 * 60 * 60);
        ArgumentCaptor<ReceiveMessageRequest> argumentCaptor = ArgumentCaptor.forClass(ReceiveMessageRequest.class);
        List<Message> messages = new ArrayList<>();
        ReceiveMessageResult result = new ReceiveMessageResult().withMessages(messages);
        mockCreateQueueResult(queueUrl, actualAwsQueueUrl); //mock create queue

        //when
        doReturn(result).when(client).receiveMessage(any(ReceiveMessageRequest.class));
        service.pull(queueUrl, visibilityTimeout);

        //then
        verify(client).receiveMessage(argumentCaptor.capture());
        assertEquals(1, argumentCaptor.getAllValues().size());

        ReceiveMessageRequest receiveMessageRequest = argumentCaptor.getAllValues().get(0);
        assertEquals(actualAwsQueueUrl, receiveMessageRequest.getQueueUrl()); //assert it pulls from aws actual url
        assertEquals(Integer.valueOf(1), receiveMessageRequest.getMaxNumberOfMessages());
        assertEquals(visibilityTimeout, receiveMessageRequest.getVisibilityTimeout());
    }

    @Test(timeout = 500)
    public void check_delete() throws IOException {
        //given
        String queueUrl = "queue-for-delete";
        String actualAwsQueueUrl = Joiner.on("/").join("aws", queueUrl);
        String receiptHandle = "this-is-a-receiptHandle-key";
        ArgumentCaptor<DeleteMessageRequest> argumentCaptor = ArgumentCaptor.forClass(DeleteMessageRequest.class);
        mockCreateQueueResult(queueUrl, actualAwsQueueUrl); //mock create queue

        //when
        service.delete(queueUrl, receiptHandle);

        //then
        verify(client).deleteMessage(argumentCaptor.capture());
        assertEquals(1, argumentCaptor.getAllValues().size());

        DeleteMessageRequest input = argumentCaptor.getAllValues().get(0);
        assertEquals(actualAwsQueueUrl, input.getQueueUrl()); //assert it deletes from aws actual url
        assertEquals(receiptHandle, input.getReceiptHandle());
    }

    /**
     * Helper Mock to just stub queueUrl mock to return same queueUrl
     *
     * @param queueUrl
     */
    private void mockCreateQueueResult(String queueUrl, String amazonQueueUrl) {
        CreateQueueResult result = mock(CreateQueueResult.class);
        doReturn(result).when(client).createQueue(eq(queueUrl));
        doReturn(amazonQueueUrl).when(result).getQueueUrl();
    }
}